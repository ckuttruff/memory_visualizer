require 'json'
require 'sinatra'
require_relative 'lib/output_parser'

get '/' do
  results = OutputParser.new.process
  erb :results, locals: { json_data: results.to_json }
end
